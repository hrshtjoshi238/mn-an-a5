import React,{Component} from 'react';
class ProductTable extends Component{
    state={
        products:[
            {"product":"Pepsi", "sales":[2,5,8,10,5]},
            {"product":"Coke", "sales":[3,6,5,4,11,5]},
            {"product":"5Star", "sales":[10,14,22]},
            {"product":"Maggi", "sales":[3,3,3,3,3]},
            {"product":"Perk", "sales":[1,2,1,2,1,2]},
            {"product":"Bingo", "sales":[0,1,0,3,2,6]},
            {"product":"Gems", "sales":[3,3,1,1]}
        ],
        details:-1
    }
    sortArr=(num)=>{
        console.log(num);
        let s1={...this.state};
        if(num===0){
            s1.products.sort((x,y)=>x.product.localeCompare(y.product));
        }
        else if(num===1){
            s1.products.sort((x,y)=>parseInt(this.getSales(x.sales))-parseInt(this.getSales(y.sales)));
        }
        else{
            s1.products.sort((x,y)=>this.getSales(y.sales)-this.getSales(x.sales));
        }
        this.setState(s1);
    }
    getSales=(arr)=>arr.reduce((acc,curr)=>acc+=curr,0);
    render(){
        let {products,details}=this.state;
        return(
            <div className="container p-2 text-center">
                <button
                className="btn btn-primary ml-1"
                onClick={()=>this.sortArr(0)}
                >
                    Sort By Product
                </button>
                <button
                className="btn btn-primary ml-1"
                onClick={()=>this.sortArr(1)}
                >
                    Total Sales Asc
                </button>
                <button
                className="btn btn-primary ml-1"
                onClick={()=>this.sortArr(2)}
                >
                    Total Sales Desc
                </button>
                <div className="row bg-dark text-white mt-4">
                    <div className="col-4 border">Product</div>
                    <div className="col-4 border">Total Sales</div>
                    <div className="col-4 border">Details</div>
                </div>
                {
                    products.map((x,index)=>{
                        let {product,sales}=x
                        return(<div className="row">
                            <div className="col-4 border">{product}</div>
                            <div className="col-4 border">{this.getSales(sales)}</div>
                            <div className="col-4 border"><button className="btn btn-info btn-sm"
                            onClick={()=>{
                                let s1={...this.state};
                                s1.details=index;
                                this.setState(s1);
                            }}>Details</button></div>
                            </div>
                            )
                    })
                }
                <div>
                   {
                       details!=-1?(
                           <React.Fragment>
                           <div className="text-left">
                           <h4>Sales of {products[details].product}</h4>
                           <ul>
                               {
                                   products[details].sales.map((x)=><li>{x}</li>)
                               }
                           </ul>
                           </div>
                           </React.Fragment>
                       ):""
                   } 
                </div>
            </div>
        )
    }
}
export default ProductTable;