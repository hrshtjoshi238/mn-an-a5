import React,{Component} from 'react';
class Participents extends Component{
    render(){
        let {player,buzzer}=this.props;
        return(
            <div className={
                buzzer===null||buzzer!=player?"p-2 pl-4 pr-4 bg-warning m-3":"p-2 pl-4 pr-4 bg-success m-3"
                }>
                Name : {player.name}<br/>
                Score : {player.points}<br/>
                <button className="p-2"
                disabled={buzzer?"true":""}
                onClick={()=>this.props.onSubmit(player)}>
                    Buzzer
                </button>
            </div>
        )
    }
}
export default Participents;