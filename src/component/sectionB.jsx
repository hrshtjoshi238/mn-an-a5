import React,{Component} from 'react';
import Participents from './participent';
import {getQuizQuestions} from './quizqns';
class QuizShow extends Component{
    state={
        players:[
            {
                name:"James",
                points:0
            },
            {
                name:"Julia",
                points:0
            },
            {
                name:"Martha",
                points:0
            },
            {
                name:"Steve",
                points:0
            }
        ],
        questions:getQuizQuestions(),
        currentQuestion:0,
        playerBuzzer:null
    }
    buzzerHandle=(player)=>{
        let s1={...this.state};
        s1.playerBuzzer=player;
        this.setState(s1);
    }
    checkAns=(ansIndex)=>{
        let s1={...this.state};
        let index=s1.players.findIndex((x)=>x.name===s1.playerBuzzer.name);
        if(s1.questions[s1.currentQuestion].answer===ansIndex){
            s1.players[index].points+=3
            alert("Correct Answer!You get 3 points");
        }
        else{
            s1.players[index].points-=1;
            alert("Incorrect Answer!You loose 1 point");
        }
        s1.currentQuestion++;
        s1.playerBuzzer=null;
        this.setState(s1);
    }
    checkResult(){
        let {players}=this.state;
        let arr=players;
        let temp=[];
        arr.sort((x,y)=>y.points-x.points);
        for(let i=1;i<arr.length;i++){
            if(arr[i].points===arr[0].points){
                temp.push(arr[i].name);
            }
        }
        temp.push(arr[0].name);
        return temp.length===1?"The Winner is "+temp[0]
        :"There is a tie. The winners are "+temp.join(",");
    }
    render(){
        let {players,questions,currentQuestion,playerBuzzer} =this.state;
        return(
            <div className="container text-center">
                <h1>Welcome to the Quiz Contest</h1>
                <h5>Participents</h5>
                <div className="div d-flex justify-content-center">
                {
                    players.map((x,index)=>(
                        <Participents
                        player={x}
                        buzzer={playerBuzzer}
                        onSubmit={this.buzzerHandle}/>
                    ))
                }
                </div>
                {currentQuestion+1<=questions.length?
                <React.Fragment><h4>Question Number:{currentQuestion+1}</h4>
                <h5>{questions[currentQuestion].text}</h5>
                <div className="div text-center">
                {
                    questions[currentQuestion].options.map((x,index)=>(
                        <button className="btn btn-info m-2"
                        disabled={!playerBuzzer?"true":""}
                        onClick={()=>this.checkAns(index+1)}>
                            {x}
                        </button>
                    ))
                }
                </div>
                </React.Fragment>
                :<React.Fragment>
                <h4>Game Over</h4>
                <b>{this.checkResult()}</b>
                </React.Fragment>

                }
            </div>
        )
    }
}
export default QuizShow;